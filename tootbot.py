
import os.path
import os
import re
import sqlite3
import ssl
import sys
from datetime import datetime, timedelta
import pycurl
from io import BytesIO
import re
import subprocess
import magic

import feedparser
import requests
from mastodon import Mastodon

import helpers
if hasattr(ssl, '_create_unverified_context'):
    ssl._create_default_https_context = ssl._create_unverified_context

if len(sys.argv) < 4:
    print("Usage: python3 tootbot.py twitter_account mastodon_login mastodon_passwd mastodon_instance")
    sys.exit(1)

# sqlite db to store processed tweets (and corresponding toots ids)
sql = sqlite3.connect('tootbot.db')
db = sql.cursor()
db.execute('''CREATE TABLE IF NOT EXISTS tweets (tweet text, toot text, twitter text, mastodon text, instance text)''')

if len(sys.argv)>4:
    instance = sys.argv[4]
else:
    instance = 'amicale.net'

if len(sys.argv)>5:
    trigger = sys.argv[5]
else:
    trigger = None


days = 1

twitter = sys.argv[1]
mastodon = sys.argv[2]
passwd = sys.argv[3]
nitterinstance="nitter.eu"
print("Used nitter instance is: "+nitterinstance)
print("Mastodon instance is: "+instance)

mastodon_api = None

url=('https://'+nitterinstance+'/'+twitter+'/rss')

p = feedparser.parse(url)
if not p['entries'] and "not well-formed" in str(p['bozo_exception']):
    rss1 = requests.get(url).content.decode("utf-16")
    rss1 = rss1.replace("utf-16", "unicode")
    p = feedparser.parse(rss1)
entries = p['entries']


for t in reversed(entries):
    # extract number from t.id
    tid=re.findall('\d+',t.id)[-1]
    # check if this tweet has been processed already
    db.execute('SELECT * FROM tweets WHERE tweet = ? AND twitter = ?  and mastodon = ? and instance = ?',(tid, twitter, mastodon, instance))
    print("tweet id that is checked: "+tid)
    last = db.fetchone()

    # process only unprocessed tweets less than $days days old
    if last is None and (datetime.now()-datetime(t.published_parsed.tm_year, t.published_parsed.tm_mon, t.published_parsed.tm_mday, t.published_parsed.tm_hour, t.published_parsed.tm_min, t.published_parsed.tm_sec) < timedelta(days=days)):
        if mastodon_api is None:
            # Create application if it does not exist
            if not os.path.isfile(instance+'.secret'):
                if Mastodon.create_app(
                    'tootbot',
                    api_base_url='https://'+instance,
                    to_file = instance+'.secret'
                ):
                    print('tootbot app created on instance '+instance)
                else:
                    print('failed to create app on instance '+instance)
                    sys.exit(1)

            try:
                mastodon_api = Mastodon(
                  client_id=instance+'.secret',
                  api_base_url='https://'+instance
                )
                mastodon_api.log_in(
                    username=mastodon,
                    password=passwd,
                    scopes=['read', 'write'],
                    to_file=mastodon+".secret"
                )
            except:
                print("ERROR: First Login Failed!")
                sys.exit(1)
        print("Processed tweet id: "+tid)
        c = t.title
        #only tweets by the account itself (no retweets)
        if t.author != '@%s' % twitter:
            continue

        if c[0:5] == "Pinned":
            #TODO: manage pinned tweets/toots
            continue

	# enable threads
        inreplyto=None
        if re.match("R to @"+twitter+".*",c) is not None:
          print("Twitter id with thread: "+tid)
          inreplyto = helpers.preceeding_tootid(nitterinstance,db,twitter,tid)
          if inreplyto is not None:
            c = c.replace("R to @"+twitter+": ","")
          else:
            print("No preceeding toot found")
        # replace twitter handles by nitter links
        c = c.replace("@","https://"+nitterinstance+"/")
	# be a well behaved bot: add the #bot tag so people can filter you if they don't want to read bot posts
        c = c + ("\r\n I am a [#bot]\r\n original post: "+t.id)
        print("Content of the tweet:\r\n"+c)
        toot_media = []
        
        # get the pictures...
        for p in re.finditer(r"http://"+nitterinstance+"/pic[^ \xa0\"]*", t.summary):
            #check if video is attached
            isVideo = helpers.uploadVideo(nitterinstance,twitter,tid,mastodon_api,toot_media)
            if not isVideo:
               print("No video has been found")
               media = requests.get(p.group(0))
               media_posted = mastodon_api.media_post(media.content, mime_type=media.headers.get('content-type'), description="Leider kann der Bot keine Bildbeschreibungen bieten")
               toot_media.append(media_posted['id'])
             
        # replace t.co link by original URL
        m = re.search(r"http[^ \xa0]*", c)
        if m != None:
            l = m.group(0)
            r = requests.get(l, allow_redirects=False)
            if r.status_code in {301,302}:
                c = c.replace(l,r.headers.get('Location'))

        # remove ellipsis
        c = c.replace('\xa0…',' ')
 
	#you can use the spoiler_text for a automatic content warning

        if toot_media is not None:
            if trigger is not None:
                spoiler_text=trigger
            else:
                spoiler_text=None
            toot = mastodon_api.status_post(c, in_reply_to_id=inreplyto, media_ids=toot_media, sensitive=False, visibility='unlisted', spoiler_text=spoiler_text)
            if "id" in toot:
                db.execute("INSERT INTO tweets VALUES ( ? , ? , ? , ? , ? )",
                (tid, toot["id"], twitter, mastodon, instance))
                sql.commit()
 	    # newsbots.eu only allows one bot post at a time, so if we posted a tweet we will stop the script, no matter what. if you want to toot every new tweet, remove the "break"
            break
